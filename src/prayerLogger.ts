type ContextType = Record<string, any>

type LogMethod = (context: ContextType, message?: string) => void

export interface PrayerLogger {
    trace: LogMethod
    debug: LogMethod
    info: LogMethod
    warn: LogMethod
    error: LogMethod
    fatal: LogMethod
}

export class ConsoleLogger implements PrayerLogger {
    private format(lvl: string, ctx: ContextType, msg?: string) {
        return `[${lvl}] ${msg ?? ''}\n${JSON.stringify(ctx, undefined, 4)}`
    }

    trace(context: ContextType, message?: string) {
        console.trace(this.format('trace', context, message))
    }

    debug(context: ContextType, message?: string) {
        console.debug(this.format('debug', context, message))
    }

    info(context: ContextType, message?: string) {
        console.info(this.format('info', context, message))
    }

    warn(context: ContextType, message?: string) {
        console.warn(this.format('WARN', context, message))
    }

    error(context: ContextType, message?: string) {
        console.error(this.format('ERROR', context, message))
    }

    fatal(context: ContextType, message?: string) {
        console.error(this.format('FATAL', context, message))
    }
}

export class NullLogger implements PrayerLogger {
    trace(_: ContextType, __?: string) {}

    debug(_: ContextType, __?: string) {}

    info(_: ContextType, __?: string) {}

    warn(_: ContextType, __?: string) {}

    error(_: ContextType, __?: string) {}

    fatal(_: ContextType, __?: string) {}
}
