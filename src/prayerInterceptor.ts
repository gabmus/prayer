import { PrayerRequestData } from "./prayerRequestData"

export type RequestInterceptor = (reqData: PrayerRequestData) => Promise<PrayerRequestData>
export type ResponseInterceptor = (res: Response) => Promise<Response>
