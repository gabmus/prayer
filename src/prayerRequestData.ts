export type PrayerRequestData = {
    reqInit?: RequestInit,
    queryParams?: Record<string, string>
}
