import { RequestInterceptor, ResponseInterceptor } from './prayerInterceptor'
import { ConsoleLogger, PrayerLogger } from './prayerLogger'
import { PrayerRequestData } from './prayerRequestData'

type ReqInitConstructorFields = Partial<
    Pick<RequestInit, 'cache'> &
        Pick<RequestInit, 'credentials'> &
        Pick<RequestInit, 'headers'> &
        Pick<RequestInit, 'keepalive'> &
        Pick<RequestInit, 'mode'> &
        Pick<RequestInit, 'redirect'> &
        Pick<RequestInit, 'referrer'> &
        Pick<RequestInit, 'referrerPolicy'>
>

export type PrayerClientConstructorParams = {
    logger?: PrayerLogger
    reqInterceptors?: RequestInterceptor[]
    resInterceptors?: ResponseInterceptor[]
    reqOptions?: ReqInitConstructorFields
}

export class PrayerClient {
    protected readonly logger: PrayerLogger

    private readonly reqInterceptors: RequestInterceptor[]

    private readonly resInterceptors: ResponseInterceptor[]

    private readonly reqOptions: RequestInit

    constructor({
        logger = new ConsoleLogger(),
        reqInterceptors = [],
        resInterceptors = [],
        reqOptions = {},
    }: PrayerClientConstructorParams = {}) {
        this.logger = logger
        this.reqInterceptors = reqInterceptors
        this.resInterceptors = resInterceptors
        this.reqOptions = reqOptions
    }

    private withQueryParams(url: URL, params?: Record<string, string>): URL {
        if (params) {
            Object.entries(params).forEach(([key, val]) => {
                url.searchParams.set(key, val)
            })
        }
        return url
    }

    private setReqInit(reqInit: RequestInit): RequestInit {
        return {
            ...reqInit,
            credentials: reqInit.credentials ?? this.reqOptions.credentials,
            headers: { ...this.reqOptions.headers, ...reqInit.headers },
            keepalive: reqInit.keepalive ?? this.reqOptions.keepalive,
            mode: reqInit.mode ?? this.reqOptions.mode,
            redirect: reqInit.redirect ?? this.reqOptions.redirect,
            referrer: reqInit.referrer ?? this.reqOptions.referrer,
            referrerPolicy:
                reqInit.referrerPolicy ?? this.reqOptions.referrerPolicy,
        }
    }

    async request(
        _url: URL | string,
        _data: PrayerRequestData = {}
    ): Promise<Response> {
        const url = _url instanceof URL ? _url : new URL(_url)
        let data: PrayerRequestData = {
            reqInit: _data.reqInit ?? {},
            queryParams: _data.queryParams ?? {},
        }
        data.reqInit = this.setReqInit(data.reqInit!)
        for (const i of this.reqInterceptors) {
            data = await i(data)
        }
        const urlWithParams = this.withQueryParams(url)
        this.logger.info({ event: 'request', url: urlWithParams, data: data })

        let res = await fetch(urlWithParams, data.reqInit)

        const logRes = this.logger[res.ok ? 'info' : 'error']
        logRes({
            event: 'response',
            url: urlWithParams,
            data: data,
            response: res,
        })
        for (const i of this.resInterceptors) {
            res = await i(res)
        }
        if (this.resInterceptors.length > 0) {
            logRes({
                event: 'response_post_interceptors',
                url: urlWithParams,
                data: data,
                response: res,
            })
        }

        return res
    }

    private requestWithMethod(
        url: URL | string,
        method: string,
        data?: PrayerRequestData
    ): Promise<Response> {
        return this.request(url, {
            reqInit: { ...data?.reqInit, method },
            queryParams: data?.queryParams ?? {},
        })
    }

    get(url: URL | string, data?: PrayerRequestData): Promise<Response> {
        return this.requestWithMethod(url, 'GET', data)
    }

    head(url: URL | string, data?: PrayerRequestData): Promise<Response> {
        return this.requestWithMethod(url, 'HEAD', data)
    }

    post(url: URL | string, data?: PrayerRequestData): Promise<Response> {
        return this.requestWithMethod(url, 'POST', data)
    }

    put(url: URL | string, data?: PrayerRequestData): Promise<Response> {
        return this.requestWithMethod(url, 'PUT', data)
    }

    delete(url: URL | string, data?: PrayerRequestData): Promise<Response> {
        return this.requestWithMethod(url, 'DELETE', data)
    }

    connect(url: URL | string, data?: PrayerRequestData): Promise<Response> {
        return this.requestWithMethod(url, 'CONNECT', data)
    }

    options(url: URL | string, data?: PrayerRequestData): Promise<Response> {
        return this.requestWithMethod(url, 'OPTIONS', data)
    }

    trace(url: URL | string, data?: PrayerRequestData): Promise<Response> {
        return this.requestWithMethod(url, 'TRACE', data)
    }

    PATCH(url: URL | string, data?: PrayerRequestData): Promise<Response> {
        return this.requestWithMethod(url, 'PATCH', data)
    }
}
