# Prayer

HTTP client with interceptor and logging support.

Requests are done using the fetch API.

## Install

```bash
npm i --save git+https://gitlab.com/gabmus/prayer.git#release
```

## Usage

Generic out of the box usage:

```typescript
import { PrayerClient } from 'prayer'

const prayer = new PrayerClient()

prayer.get('https://example.org').then(res => {
    if (res.ok) return res.text()
}).then(res => {
    console.log(res)
}).catch(err => {
    console.error(err)
})
```

With [pino](https://github.com/pinojs/pino) (custom loggers can be used by implementing the `PrayerLogger` interface):

```typescript
import { PrayerClient } from 'prayer'
import pino from 'pino'

const logger = pino()
const prayer = new PrayerClient({
    logger
})

// same general usage as above
```

With interceptors:

```typescript
import { PrayerClient } from 'prayer'
import { getAuthorizationHeaders, decryptPayload } from './myLogic'

const prayer = new PrayerClient({
    reqInterceptors: [(data) => ({...data, ...getAuthorizationHeaders()})],
    resInterceptors: [(res) => decryptPayload(res)]
})
```
